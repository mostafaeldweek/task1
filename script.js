// let arr1 = [1, 2, 3];
// let arr2 = [3, 4, 5];
// let arr3 = [5, 6, 7];

// let Rduplicate = [...new Set([...arr1, ...arr2, ...arr3])];

// console.log(Rduplicate);

/*----------------------------------------------*/

// let merge = arr1.concat(arr2, arr3);
// console.log(merge);
// console.log(Array.from(new Set(arr1.concat(arr2, arr3))));
// const mergedArray = Array.from(new Set(arr1.concat(arr2, arr3)));

// let x = [];

/*----------------------------------------------*/
// for (let index = 0; index < merge.length; index++) {
//   let isDuplicate = false;

//   for (let j = 0; j < x.length; j++) {
//     if (merge[index] === x[j]) {
//       isDuplicate = true;
//       break;
//     }
//   }

//   if (!isDuplicate) {
//     x.push(merge[index]);
//   }
// }

// console.log(x);

/*
Task 2
*/

// function calcAvg(...par) {
//   if (par.length === 0) {
//     return 0;
//   }
//   let sum = 0;
//   for (let i = 0; i < par.length; i++) {
//     sum = sum + par[i];
//   }
//   return sum / par.length;
// }
// let dResult = calcAvg(1, 2, 3, 4, 5, 6, 7);
// console.log(dResult);

// -------------------------------------------------

// task 3

function calcFactorial(par) {
  if (par === 0) {
    return 1;
  } else if (par < 0) {
    return "not a valid number";
  }

  return par * calcFactorial(par - 1);
}

const fResult = calcFactorial(-7);
console.log(fResult);

// -----------------------------------------------

// task 4

// const mainArray = [1, [1, 1], [1, 1]];

// const aShallowCopy = [...mainArray];

// const deepCopy = JSON.parse(JSON.stringify(mainArray));

// mainArray[0] = "main";
// mainArray[1][1] = "main nested";

// aShallowCopy[0] = "shallow";
// aShallowCopy[1][1] = "shallow nested";

// deepCopy[0] = "deep";
// deepCopy[1][1] = "deep Nested";

// console.log(mainArray);
// console.log(aShallowCopy);
// console.log(deepCopy);

// nested object

const mainObject = {
  first: 0,
  inside: {
    inside1: 0,
    inside2: 0,
  },
};

const oShallowCopy = { ...mainObject };

const oDeepCopy = JSON.parse(JSON.stringify(mainObject));

mainObject.first = "mainChange";
mainObject.inside.inside1 = "mainNchange";
oShallowCopy.first = "copyChange";
oShallowCopy.inside.inside1 = "copyNchange";
oDeepCopy.first = "deep";
oDeepCopy.inside.inside1 = "deep";

console.log(mainObject);
console.log(oShallowCopy);
console.log(oDeepCopy);
